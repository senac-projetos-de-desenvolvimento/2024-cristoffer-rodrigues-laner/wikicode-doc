# WIKICODE-doc

- [ ] [Prototipação de telas](https://www.figma.com/file/DuZS1a0QZJ5obi1Esk7ako/TCC-1?type=design&node-id=0%3A1&mode=design&t=mjgmbIqzjTSlH71n-1)
- [ ] [Backlog](https://trello.com/b/Fws9pql3/tcc-1)
- [ ] [Apresentação](https://www.canva.com/design/DAGLUZMaJwI/PFmBJKGWdztLoUHiCAeQaw/view?utm_content=DAGLUZMaJwI&utm_campaign=designshare&utm_medium=link&utm_source=editor)

# **Curso de Análise e Desenvolvimento de Sistemas - UNISENAC - 2024/1**
## **Integrantes**

Cristoffer Rodrigues Laner

cristofferrlaner@gmail.com

Graduando em Análise e Desenvolvimento de Sistemas

# **Orientador**

Prof. Me. Ângelo Luz

# **Motivação/Justificativa**

A cultura do imediatismo está tornando cada vez mais necessário o atendimento rápido e eficaz, segundo uma pesquisa do site [Econsultancy](https://econsultancy.com/83-of-online-shoppers-need-support-to-complete-a-purchase-stats/) 31% dos clientes desejam ajuda online instantânea, enquanto 40% deles esperam receber assistência em menos de 5 minutos.
Para isso, foram criadas as Bases de Conhecimento, visando ajudar o cliente a encontrar as respostas para suas perguntas, antes de precisar acionar o suporte da marca.
Sabendo disso, a ideia do WIKICODE surgiu, com a premissa de ser uma plataforma para centralização de informações, com a segurança e facilidade que o cliente necessita.
A plataforma será capaz de comportar publicações de versões e guias/manuais, tendo em vista a disseminação de um novo release, correções de bugs, novas funcionalidades ou o uso de ferramentas específicas do sistema.

# **Proposta de MVP**

O Wikicode visa criar uma base de conhecimento sólida que possa servir como comunicador entre o produto e usuários, assim potencializando o conhecimento e diminuindo a taxa de dúvidas. A ideia é que essa base de conhecimento possa ser acessada de qualquer lugar, a qualquer momento, tendo o gerenciamento e autenticação necessária para que seja um lugar seguro para mostrar seu produto.
A plataforma será capaz de comportar publicações de versões e guias, tendo em vista a disseminação de um novo release, correções de bugs, novas funcionalidades ou o uso de ferramentas específicas do produto.
O MVP inicialmente terá cadastro de usuários, cadastro de publicações, páginas para listagem de publicações/usuários e vinculação de pessoas às publicações.

# **Rerefencial Teórico**

## **Estado da Arte**

Base de conhecimento é o nome dado a um sistema ou plataforma capaz de centralizar dados e informações-chave sobre um determinado produto ou serviço. Quando falamos de uma base de conhecimento comercial, devemos incluir, também, processos, métodos adotados e estatísticas essenciais para uma profunda compreensão do ciclo de vendas. 

Empresas que investem em gestão de conhecimento como base de suas estratégias de negócio tendem a sair na frente. A correta administração deste valioso recurso faz toda a diferença no desenvolvimento da marca.

Nesse sentido, poder contar com uma base de dados de conhecimento dá mais segurança para a empresa no que se refere às informações sobre seus produtos, serviços e processos internos.

Você sabia que o pessoal de atendimento gasta, em média, 20% de seu tempo procurando as informações corretas, seja para responder a clientes ou mesmo para realizar suas tarefas dentro dos procedimentos definidos pela empresa?

Contar com uma base de conhecimento estruturada e fácil de consultar é uma forma de evitar esse desperdício de tempo. 

Veja estes 6 benefícios que essa base pode trazer para o seu negócio: 

- Potencializa o aprendizado
- Centraliza as informações
- Agilidade no atendimento
- Redução de custos com pessoal
- Melhoria no treinamento de novos colaboradores
- Diminuição de retrabalhos
- Maior engajamento das equipes


**Estatísticas e pesquisas a serem consideradas**

Mais de 50% dos clientes acham que é importante resolver eles próprios os problemas dos produtos, em vez de depender do atendimento ao cliente (Zendesk).

51% dos clientes preferem suporte através de uma base de conhecimento online (Econsultancy).

70% dos clientes preferem usar o site de uma empresa para obter respostas às suas perguntas em vez de usar telefone ou e-mail (Forrester).

55% afirmam que o fácil acesso à informação e ao suporte pode fazer com que se apaixonem por uma marca (RightNow).

45% das organizações que oferecem opções de autoatendimento web ou móvel notaram um aumento no tráfego do site e uma redução nas consultas telefônicas (CRM Magazine).

50% das organizações que oferecem suporte social ao cliente observaram um aumento no índice de satisfação do cliente (Ciboodle).


## **Análise de Concorrentes**

**Live Agent**

Em 2004, os fundadores, Viktor e Andrej, criaram uma empresa de desenvolvimento de software chamada Quality Unit. Eles perceberam que a comunicação com seus clientes costumava ser ineficiente, então procuraram um software que os ajudasse a fornecer atendimento ao cliente em tempo real. Descobrindo que nenhuma solução poderia atender às suas necessidades, eles decidiram desenvolver o LiveAgent – uma ferramenta de suporte ao cliente repleta de recursos de chat ao vivo, emissão de tickets e help desk.

**Confluence da Atlassian**

Confluence é um wiki empresarial baseado na web desenvolvido pela empresa de software australiana Atlassian. O Confluence é uma ferramenta de documentação corporativa essencial para empresas que buscam a otimização do trabalho colaborativo. Ela permite a interação por meio do armazenamento e compartilhamento de informações reunidas em uma só plataforma, oferecendo acesso à informação de forma centralizada.

**Comparativo**

Se tratando de comparativos, no geral, as bases de conhecimento dos sistemas similares são bem próximos ao do WIKICODE, o que torna os sistemas similares mais robustos é a geração de uma página de cliente com as postagens públicas, além delas fornecerem outros serviços de emissão de tickets e help desk.

De toda forma, o WIKICODE pretende expandir seus horizontes até as páginas de clientes, tornando o sistema mais completo para empresas que desejam tornar suas postagens públicas.

# **Requisitos Funcionais**

- RF001 - Fazer login de usuário
    - RN - A autenticação se dará por meio de email e senha,  que precisará de pelo menos 8 dígitos, com letras, números e caracteres especiais.
- RF002 - Cadastrar, listar, alterar e excluir usuários
    - RN - O CUD de usuários será feito somente pelo usuário Master.
- RF003 - Cadastrar, listar, alterar e excluir publicações
    - RN - O CUD de publicações será feito somente pelos usuários Editores.
- RF004 - Listar publicações mais recentes ao topo
    - RN - O select será feito com base na data, trazendo os mais recentes primeiro.
- RF005 - Vincular publicações a usuários
    - RN - Terá uma tabela auxiliar no banco para relacionar usuário à publicação.
- RF006 - Diferenciar funções e permissões de usuários
    - RN - Ao logar o sistema deverá mostrar ao usuário somente os módulos que ele tem acesso.
- RF007 - Impedir usuários leitores de cadastrar, alterar ou excluir publicações
 - RN - Será validado pela API qual a função do usuário e por sua vez as suas permissões.
- RF008 - Permitir que usuários editores possam cadastrar e alterar publicações
    - RN - Será validado pela API qual a função do usuário e por sua vez as suas permissões.
- RF009 - Permitir que somente o usuário master exclua publicações
    - RN - Será validado pela API qual a função do usuário e por sua vez as suas permissões.
- RF010 - Permitir que somente o usuário master gerencie usuários
    - RN - Será validado pela API qual a função do usuário e por sua vez as suas permissões.
- RF011 - Fazer logoff de usuário
    - RN - O sistema deve finalizar a sessão do usuário e voltar a tela de login.

# **Requisitos Não Funcionais**

- RNF1 Qualquer listagem não deve demorar mais que 2s
- RNF2 Os erros e exceções devem ter mensagens personalizadas e amigáveis
- RNF3 Deve proteger contra acesso não autorizado
- RNF4 As senhas precisam ser criptografadas e complexas
- RNF5 Os usuários e publicações não devem ser deletados permanentemente do banco de dados
- RNF6 Deve ser responsivo a diferentes tamanhos de telas
- RNF7 A API deve seguir a arquitetura REST

# **Tecnologias Utilizadas**
- Frontend
    - NEXTJS
    - BOOTSTRAP
    - CSS
    - BIBLIOTECAS REACT

- BACKEND
    - EXPRESS
    - SEQUELIZE
    - POSTGRESQL

# **Diagrama de Casos de Uso**

![alt text](<Diagrama em branco.png>)

# **Diagrama de Entidade Relacionamento**

![alt text](image.png)

## **Referências**
https://econsultancy.com/83-of-online-shoppers-need-support-to-complete-a-purchase-stats/

http://cdn.zendesk.com/resources/whitepapers/Omnichannel-Customer-Service-Gap.pdf

https://www.oracle.com/us/corporate/analystreports/enterprise-application/forrester-tei-atg-359304.pdf

https://pt.slideshare.net/slideshow/2011-customer-experience-impact-report/10947124

https://www.destinationcrm.com/Articles/ReadArticle.aspx?ArticleID=90678

https://www.ciosummits.com/media/pdf/solution_spotlight/ThinkJar_WP_US_KANA.pdf